package fountain

import (
	"bufio"
	"os"
	"regexp"
)

type Pattern string

const (
	PatternTitle Pattern = `^((Title|Credit|Author|Source|Draft\s+Date|Contact)\:)`

	PatternPageBreak     Pattern = `^>**(.+?)**<$`
	PatternScenes        Pattern = `^(INT\.?|EXT\.?|EST\.|INT\.\/EXT\.?|I/E\.?)\s+(.+)(\s+\-\s+(DAY|NIGHT))?\s+(#([0-9A-Za-z]+)#)$`
	PowerScenes          Pattern = `\#[0-9A-Za-z]\#$`
	PatternTransition    Pattern = `^((CUT\s+)?TO\:)(.+)$`
	PowerTransition      Pattern = `^\>(.+)$`
	PatternSection       Pattern = `^\#(.+)$`
	PatternSynopse       Pattern = `^\=(.+)$`
	PatternCharacter     Pattern = `^([A-Z0-9]+?)\s+(\((.+?)\))?(\^)?$`
	PowerCharacter       Pattern = `^\@(.+)$`
	PatternParenthetical Pattern = `^\((.+?)\)$`
	PatternLyrics        Pattern = `^\~(.+)$`
	PatternCentered      Pattern = `^>(.+?)<$`
	PatternAction        Pattern = `^(.+?)(\^)?$`
	PowerAction          Pattern = `^\!(.+)$`

	PatternNoteStart     Pattern = `^(\[\[|\{)`
	PatternNoteEnd       Pattern = `(\]\]|\})$`
	PatternBoneyardStart Pattern = `^/\*`
	PatternBoneyardEnd   Pattern = `\*/$`

	PatternBlod      Pattern = `**(.+?)**`
	PatternItalicize Pattern = `*(.+?)*`
	PatternUnderline Pattern = `_(*.+?)_`
	PatternNumber    Pattern = `#([0-9A-Za-z]+)\s*$`
)

var (
	BlockPatterns map[string][2]Pattern = map[string][2]Pattern{
		"Note":     [2]Pattern{PatternNoteStart, PatternNoteEnd},
		"Boneyard": [2]Pattern{PatternBoneyardStart, PatternBoneyardEnd},
	}
	NormalPatterns map[string]Pattern = map[string]Pattern{
		"Title":         PatternTitle,
		"PageBreak":     PatternPageBreak,
		"Scene":         PatternScenes,
		"Transition":    PatternTransition,
		"Synopse":       PatternSynopse,
		"Character":     PatternCharacter,
		"Parenthetical": PatternParenthetical,
		"Lyrics":        PatternLyrics,
		"Centered":      PatternCentered,
	}
	PowerPatterns map[string]Pattern = map[string]Pattern{
		"Scene":      PowerScenes,
		"Transition": PowerTransition,
		"Character":  PowerCharacter,
	}
	ActionPattern [2]Pattern = [2]Pattern{
		PatternAction, PowerAction,
	}
)

type ParsedBlock struct {
	Type   string
	Parsed []string
	Source string
}

func Nl2Br(source string) string {
	return regexp.MustCompile(`(\n\r|\r\n|[\n|\r])`).ReplaceAllString(source, "<br/>")
}
func ParseFirst(source string) (ret []ParsedBlock, err error) {
	ret = []ParsedBlock{}
	// 逐行掃瞄fountain原檔
	fSource, err := os.Open(source)
	if err != nil {
		return
	}
	defer fSource.Close()
	fileScan := bufio.NewScanner(fSource)
	fileScan.Split(bufio.ScanLines)
	var (
		parseBlock *ParsedBlock = nil
		parsing    bool         = false
		inBlock    bool         = false
	)
	for fileScan.Scan() {
		lineContent := fileScan.Text()
		if inBlock && parseBlock != nil {
			parseBlock.Source += "\n" + lineContent
			parseBlock.Parsed = append(parseBlock.Parsed, lineContent)
			continue
		}

		if parsing {
			if parseBlock != nil {
				parseBlock.Source += "\n" + lineContent
			}
		}
	}
	return
}
func parseFirst(source string) (res []ParsedBlock, err error) {
	res = []ParsedBlock{}
	// 逐行掃瞄fountain原檔
	fSource, err := os.Open(source)
	if err != nil {
		return
	}
	fileScan := bufio.NewScanner(fSource)
	fileScan.Split(bufio.ScanLines)
	var (
		inblock   bool = false //是否在解析區塊中，用於Boneyard,Note
		inparsing bool = false //是否在解譯中，
		//intrans   bool         = false //是否在解譯轉場中
		block *ParsedBlock //正在解譯的區塊
	)
	for fileScan.Scan() {
		lineContent := fileScan.Text()
		if inblock {
			if block != nil {
				switch block.Type {
				case "Note":
					reg1 := regexp.MustCompile(string(PatternNoteEnd))
					if reg1.MatchString(lineContent) {
						block.Parsed = append(block.Parsed, reg1.ReplaceAllString(lineContent, ""))
						inblock = false
						block.Source += "\n" + lineContent
						res = append(res, *block)
						block = nil
					} else {
						block.Parsed = append(block.Parsed, lineContent)
					}
				case "Boneyard":
					reg1 := regexp.MustCompile(string(PatternBoneyardEnd))
					if reg1.MatchString(lineContent) {
						block.Parsed = append(block.Parsed, reg1.ReplaceAllString(lineContent, ""))
						inblock = false
						block.Source += "\n" + lineContent
						res = append(res, *block)
						block = nil
					} else {
						block.Parsed = append(block.Parsed, lineContent)
					}
				}
			}
			continue
		}
		if inparsing {
			if regexp.MustCompile(`\s`).ReplaceAllString(lineContent, "") == "" {
				inparsing = false
				res = append(res, *block)
				block = nil
				continue
			}
			block.Parsed = append(block.Parsed, lineContent)
			block.Source += "\n" + lineContent
			continue
		}
		// 先解譯 Boneyard / Note
		reg1 := regexp.MustCompile(string(PatternBoneyardStart))
		if reg1.MatchString(lineContent) {
			block = &ParsedBlock{
				Type:   "Boneyard",
				Source: lineContent,
				Parsed: []string{reg1.ReplaceAllString(lineContent, "")},
			}
			inblock = true
			continue
		}
		reg1 = regexp.MustCompile(string(PatternNoteStart))
		if reg1.MatchString(lineContent) {
			block = &ParsedBlock{
				Type:   "Note",
				Source: lineContent,
				Parsed: []string{reg1.ReplaceAllString(lineContent, "")},
			}
			inblock = true
			continue
		}
		//解譯 PatternTitle
		if block.Type == "Title" {
			block.Type += "\n" + lineContent
			block.Parsed = append(block.Parsed, lineContent)
			continue
		}
		reg1 = regexp.MustCompile(string(PatternTitle))
		if reg1.MatchString(lineContent) {
			if block != nil {
				res = append(res, *block)
				block = nil
			}
			block = &ParsedBlock{
				Type:   "Title",
				Source: lineContent,
				Parsed: []string{reg1.ReplaceAllString(lineContent, "")},
			}
		}
		reg1 = regexp.MustCompile(string(PatternPageBreak))
		/* 解譯
		PatternPageBreak, PatternScenes, PatternTransition, PatternSection, PatternSynopse,
		PowerScenes, PowerTransition,
		*/
	}
	return
}
